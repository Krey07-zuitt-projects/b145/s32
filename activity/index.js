
const express = require('express');
const mongoose = require('mongoose');
const app = express();
const port = 4000;

mongoose.connect("mongodb+srv://krey07:DBuser7@wdco28-course-booking.9nho6.mongodb.net/session32Activity?retryWrites=true&w=majority", 
	{
		useNewUrlParser: true,
		useUnifiedTopology: true
	}
);

let db = mongoose.connection;
db.on("error", console.error.bind(console, "There is a problem with your database connection"));
db.once("open", () => console.log("You have successfully connected to your database"));

app.use(express.json());
app.use(express.urlencoded({extended: true}));

// 1-2

const userSchema = new mongoose.Schema({

	email: String,
	username: String,
	password: String,
	age: Number,
	isAdmin: {
		type: Boolean,
		default: false
	}
});

const User = mongoose.model("User", userSchema);

// 3-5

app.post("/users/signup", (req, res) => {

	User.findOne({email: req.body.email}, (err, result) => {

		if(result != null && result.email === req.body.email) {

			return res.send(`Email is already in used`)

		} else {

			let newUser = new User ({
				email: req.body.email,
				username: req.body.username,
				password: req.body.password,
				age: req.body.age,
			});

			newUser.save((saveErr, savedUser) => {
				if(saveErr){
					return console.error(saveErr)
				} else {
					return res.status(200).send(`New user has been added successfully`)
				};
			});
		};
	});
});

// 6-7

app.get("/users", (req, res) => {

	User.find({}, (err, result) => {
		if(err) {
			return console.log(err);
		} else {
			return res.status(200).json({
				users : result
			})
		}
	})
})


// 8-9

app.put("/users/update-user/:userId", (req, res) => {

	let userId = req.params.userId
	let username = req.body.username

	User.findByIdAndUpdate(userId, {username: username}, (err, updatedUsername) => {
		if(err){
			console.log(err)
		} else {
			res.send(`Congratulations the username has been updated`);
		}
	})
})

// 10-11

app.delete("/users/archive-task/:userId", (req, res) => {

	let userId = req.params.userId;

	User.findByIdAndDelete(userId, (err, deletedUser) => {
		if(err) {
			console.log(err)
		} else {
			res.send(`${deletedUser} has been deleted`)
		}
	})
})


app.listen(port, () => console.log(`My server is running successfully running at port ${port}`))