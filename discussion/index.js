
const express = require('express');
const mongoose = require('mongoose')
const newDataBase = express();
const port = 4000;

// Connecting MongoDB
mongoose.connect("mongodb+srv://krey07:DBuser7@wdco28-course-booking.9nho6.mongodb.net/session32?retryWrites=true&w=majority", 
	{
		useNewUrlParser: true,
		useUnifiedTopology: true
	}
);

let db = mongoose.connection;
db.on("error", console.error.bind(console, "There is an error with the connection"));
db.once("open", () => console.log("Successfully connected to the database"));


newDataBase.use(express.json());
newDataBase.use(express.urlencoded({extended: true}));

// Schema

const taskSchema = new mongoose.Schema({

	name: String,
	status: {
		type: String,
		default: 'pending'
	}

});

// Model

const Task = mongoose.model("Task", taskSchema);

// Business Logic

newDataBase.post("/tasks", (req, res) => {

	Task.findOne({name : req.body.name}, (err, result) => {

		if(result != null && result.name === req.body.name) {

			return res.send(`Duplicate task found: ${err}`)

		} else {
			let newTask = new Task({
				name: req.body.name
			});

			newTask.save((saveErr, savedTask) => {

				if(saveErr) {
					return console.error(saveErr)
				} else {
					return res.status(200).send(`New Task created : ${savedTask}`)
				};
			});
		};
	});
});


// Retrieving all tasks
newDataBase.get("/tasks", (req, res) => {

	Task.find({}, (err, result) => {

		if(err){
			return console.log(err);
		} else {
			return res.status(200).json({
				tasks : result
			})
		}
	})
})

// Updating task name

newDataBase.put("/tasks/update/:taskId", (req, res) => {

	let taskId = req.params.taskId
	let name = req.body.name

	Task.findByIdAndUpdate(taskId, {name: name}, (err, updatedTask) => {
		if(err){
			console.log(err)
		} else {
			res.send(`Congratulations the task has been updated`);
		}
	})
})

// DELETE

newDataBase.delete("/tasks/archive-task/:taskId", (req, res) => {

	let taskId = req.params.taskId;

	Task.findByIdAndDelete(taskId, (err, deletedTask) => {
		if(err) {
			console.log(err)
		} else {
			res.send(`${deletedTask} has been deleted`)
		}
	})
})

newDataBase.listen(port, () => console.log(`My server is running successfully running at port ${port}`))